﻿/*
Домашнее задание

Циклы и рекурсии
Цель:

В этом задании вы сами попробуете создать алгоритм использующий рекурсию и его аналог с использованием цикла. 
Опционально вы можете проверить скорость работы этих двух алгоритмов.

Описание/Пошаговая инструкция выполнения домашнего задания:

1.    Реализовать метод нахождения n-го члена последовательности Фибоначчи по формуле F(n) = F(n-1) + F(n-2) с помощью рекурсивных вызовов.
2.    Реализовать метод нахождения n-го члена последовательности Фибоначчи по формуле F(n) = F(n-1) + F(n-2) с помощью цикла.
3.    Добавить подсчёт времени на выполнение рекурсивного и итеративного методов с помощью Stopwatch и написать сколько времени для значений 5, 10 и 20.


Критерии оценки:

Пункт 1 - 4 балла
Пункт 2 - 4 балла
Пункт 3 - 2 балла
Минимальное количество баллов для сдачи: 8 баллов.

Рекомендуем сдать до: 11.07.2023 
*/

using System.Diagnostics;

namespace LoopsAndRecursion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // не использовать индекс > 40: Тип int не вмещает значение, а алгоритм с урока работает слишком долго.
            int elementIdxToFind = 30;
            int resultElement;
            var stopWatch = new Stopwatch();

            Console.WriteLine($"Searching for {elementIdxToFind}-th number in Fibonacci sequence...\n");

            // циклы
            stopWatch.Restart();
            resultElement = FindFiboNumByLoop(elementIdxToFind);
            stopWatch.Stop();
            Console.WriteLine($"Loop-based algorithm result is {resultElement}");
            Console.WriteLine($"Elapsed ticks: {stopWatch.ElapsedTicks}\n");

            // моя рекурсия
            stopWatch.Restart();
            resultElement = FindFiboNumByRecursion(elementIdxToFind);
            stopWatch.Stop();
            Console.WriteLine($"Recursion-based algorithm result is {resultElement}");
            Console.WriteLine($"Elapsed ticks: {stopWatch.ElapsedTicks}\n");

            // рекурсия с урока
            stopWatch.Restart();
            resultElement = FindFiboNumByRecursionLesson(elementIdxToFind);
            stopWatch.Stop();
            Console.WriteLine($"Lesson recursion-based algorithm result is {resultElement}");
            Console.WriteLine($"Elapsed ticks: {stopWatch.ElapsedTicks}\n\n");

            /*
             * Результаты измерений ( в тиках).
             * 
             * Индекс числа | Время метода с циклами | Время моего метода с рекурсией | Время метода с рекурсией с урока
             * 5            | 1059                   | 1769                           | 662
             * 10           | 1084                   | 1117                           | 804
             * 20           | 1555                   | 1841                           | 2440
             * 30           | 1532                   | 1178                           | 100024
             * 40           | 1505                   | 1190                           | 12126194
             * 
             * (получается довольно высокий разброс значений при повторных выполнениях)
             */
        }

        /// <summary>
        /// Поиск n-го члена последовательности Фибоначчи с использованием цикла
        /// </summary>
        static int FindFiboNumByLoop (int elementIdxToFind)
        {
            // Нулевой член не вычисляется по формуле, а принимается 0
            if (elementIdxToFind == 0)
                return 0;
            // Первый член не вычисляется по формуле, а принимается 1
            if (elementIdxToFind == 1)
                return 1;

            int currentFiboNum = 1, lastFiboNum = 0;

            for (int i = 2; i <= elementIdxToFind; i++)
            {
                int exchangeVar = currentFiboNum;
                checked { currentFiboNum += lastFiboNum; }
                lastFiboNum = exchangeVar;
            }
            
            return currentFiboNum; 
        }

        /// <summary>
        /// Моя реализация рекурсивного алгоритма поиска n-го числа Фибоначчи
        /// </summary>
        static int FindFiboNumByRecursion (int elementIdxToFind)
        {
            // Рекурсивно вызывается только метод в самом конце.

            // Нулевой член не вычисляется по формуле, а принимается 0
            if (elementIdxToFind == 0)
                return 0;
            // Первый член не вычисляется по формуле, а принимается 1
            if (elementIdxToFind == 1)
                return 1;

            return FindFiboNumRecursive(currentElementIdx: 1, elementIdxToFind, lastFiboNum: 0, currentFiboNum: 1);
        }

        /// <summary>
        /// Метод, вызываемый рекурсивно из FindFiboNumByRecursion() для поиска n-го члена последовательности
        /// </summary>
        static int FindFiboNumRecursive (int currentElementIdx, int elementIdxToFind, int lastFiboNum, int currentFiboNum)
        {
            int nextFiboNum;
            checked { nextFiboNum = lastFiboNum + currentFiboNum; }
            if (currentElementIdx + 1 == elementIdxToFind)
                return nextFiboNum;

            return FindFiboNumRecursive(currentElementIdx + 1, elementIdxToFind, lastFiboNum: currentFiboNum, currentFiboNum: nextFiboNum);
        }

        /// <summary>
        /// Короткая реализация рекурсивного алгоритма, предложенная на уроке
        /// </summary>
        static int FindFiboNumByRecursionLesson (int elementIdxToFind)
        {
            if (elementIdxToFind == 0)
                return 0;
            if (elementIdxToFind == 1)
                return 1;

            return FindFiboNumByRecursionLesson(elementIdxToFind - 1) + FindFiboNumByRecursionLesson(elementIdxToFind - 2);
        }
    }
}